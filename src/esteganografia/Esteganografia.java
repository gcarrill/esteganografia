package esteganografia;

import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class Esteganografia 
{

	/**
     * Devuelve un <code>BufferedImage</code> al que se le ha colocado el <code>mensaje</code> que recibe como parametro.
     *
     * @param imagen      <code>BufferedImage</code> al que se le debe colocar el <code>mensaje</code>.
     * @param mensaje     <code>String</code> con el <code>mensaje</code> a colocar en la <code>imagen</code>.
     * @return            <code>BufferedImage</code> con el <code>mensaje</code> esteganografiado.
     */
	public static BufferedImage meterMensaje(BufferedImage imagen, String mensaje)
	{		
		int[] b_mensaje = crearBytesMensaje(mensaje);		
		BufferedImage ret = modificarPixeles(imagen, b_mensaje);
		return ret;
	}

	/**
     * Devuelve un <code>Array de int</code> conteniendo el <code>mensaje</code> descompuesto en los distintos <code>bits</code> que lo conforman.
     *
     * @param mensaje     <code>String</code> con el <code>mensaje</code> a descomponer.
     * @return            <code>Array de int</code> con el <code>mensaje</code> descompuesto en <code>bits</code>.
     */
	protected static int[] crearBytesMensaje(String mensaje) 
	{
		char[] temp = mensaje.toCharArray();
		int[] aux = new int[temp.length + 1]; 
		
		for(int i = 0; i < temp.length; i++)
			aux[i] = temp[i];
		aux[aux.length -1] = 0xff;
				
		int[] ret = new int[aux.length * 8];
		
		for(int i = 0; i < aux.length; i++)
		{
			ret[(i * 8) + 0] = (aux[i] & 0x01) >>> 0;
			ret[(i * 8) + 1] = (aux[i] & 0x02) >>> 1;
			ret[(i * 8) + 2] = (aux[i] & 0x04) >>> 2;
			ret[(i * 8) + 3] = (aux[i] & 0x08) >>> 3;
			ret[(i * 8) + 4] = (aux[i] & 0x10) >>> 4;
			ret[(i * 8) + 5] = (aux[i] & 0x20) >>> 5;
			ret[(i * 8) + 6] = (aux[i] & 0x40) >>> 6;
			ret[(i * 8) + 7] = (aux[i] & 0x80) >>> 7;
		}
		return ret;
	}

	/**
     * Devuelve un <code>BufferedImage</code> al que se le han modificado los <code>pixeles</code> de manera de contener un <code>mensaje</code> esteganografiado.
     *
     * @param imagen      <code>BufferedImage</code> al que se le debe colocar el <code>mensaje</code>.
     * @param b_mensaje   <code>array de int</code> con los <code>bits</code> a colocar en los <code>colores</code> de los <code>pixeles</code> de la <code>imagen</code>.
     * @return            <code>BufferedImage</code> con <code>pixeles</code> modificados.
     */
	private static BufferedImage modificarPixeles(BufferedImage imagen, int[] b_mensaje) 
	{
		BufferedImage aux = new BufferedImage(imagen.getWidth(), imagen.getHeight(), imagen.getType());
		int cont_b = 0;
		int cont_p = 0;
		for(int i = 0; i < imagen.getWidth(); i++) 
		{
			for(int j = 0; j < imagen.getHeight(); j++)
			{
				int pixel = imagen.getRGB(i, j);
				
				if (cont_p < b_mensaje.length)
				{
					int rojo = sacarRojo(pixel);
					int verde = sacarVerde(pixel);
					int azul = sacarAzul(pixel);
					if(cont_b < b_mensaje.length)
						rojo = meterBit(rojo, b_mensaje[cont_b]);
					if(cont_b + 1 < b_mensaje.length)
						verde = meterBit(verde, b_mensaje[cont_b + 1]);
					if(cont_b + 2 < b_mensaje.length)
						azul = meterBit(azul, b_mensaje[cont_b + 2]);
					cont_b+=3;
					pixel = unirColores(rojo, verde, azul);
				}
				aux.setRGB(i, j, pixel);
				cont_p++;
			}
		}
		return aux;
	}
	
	/**
     * Devuelve un <code>int</code> con la informacion del <code>color rojo</code> del <code>pixel</code> dado.
     *
     * @param pixel       <code>pixel</code> al que se le quiere sacar el <code>color rojo</code>.
     * @return            <code>int</code> que representa el <code>color rojo</code> del <code>pixel</code>.
     */
	protected static int sacarRojo(int pixel)
	{
		pixel = pixel >>> 16 & 0x000000FF;
		return pixel;
	}
	
	/**
     * Devuelve un <code>int</code> con la informacion del <code>color verde</code> del <code>pixel</code> dado.
     *
     * @param pixel       <code>pixel</code> al que se le quiere sacar el <code>color verde</code>.
     * @return            <code>int</code> que representa el <code>color verde</code> del <code>pixel</code>.
     */
	protected static int sacarVerde(int pixel)
	{
		pixel = pixel >>> 8 & 0x000000FF;
		return  pixel;
	}
	
	/**
     * Devuelve un <code>int</code> con la informacion del <code>color azul</code> del <code>pixel</code> dado.
     *
     * @param pixel       <code>pixel</code> al que se le quiere sacar el <code>color azul</code>.
     * @return            <code>int</code> que representa el <code>color azul</code> del <code>pixel</code>.
     */
	protected static int sacarAzul(int pixel) 
	{
		pixel = pixel >>> 0 & 0x000000FF;
		return  pixel;
	}

	/**
     * Devuelve un <code>int</code> representando el <code>color</code> recibido al que se le a cambiado 
     * el <code>bit menos significante</code> de acuerdo al <code>bit</code> correspondiente del <code>mensaje</code> dado.
     *
     * @param color       <code>int</code> que representa el <code>color</code> al que se le quiere agregar el <code>bit</code> del <code>mensaje</code>.
     * @param bit_msg     <code>int</code> que contiene <code>bit</code> del <code>mensaje</code> que se quiere colocar en el <code>color</code>.
     * @return            <code>int</code> que representa el <code>color</code> con el <code>bit menos significante</code> cambiado de acuerdo al <code>bit</code> del <code>mensaje</code>.
     */
	protected static int meterBit(int color, int bit_msg) 
	{
		color = (color >>> 1);
		color = (color << 1);
		return (color ^ bit_msg);
	}
	
	/**
     * Devuelve un <code>int</code> que representa un <code>pixel</code> con la informacione de los <code>colores</code> que recive.
     *
     * @param rojo        <code>int</code> que representa el <code>color rojo</code>.
     * @param verde       <code>int</code> que representa el <code>color verde</code>.
     * @param azul        <code>int</code> que representa el <code>color azul</code>.
     * @return            <code>int</code> que representa el <code>pixel</code> formado con los tres <code>colores</code>.
     */
	protected static int unirColores(int rojo, int verde, int azul) 
	{
		return ((rojo << 16) + (verde << 8) + azul) ^ 0xff000000;
	}
	
	/**
     * Devuelve un <code>String</code> con el <code>mensaje</code> que se ha ocultado en los <code>pixeles</code> de la <code>imagen</code>.
     *
     * @param imagen      <code>BufferedImage</code> que contiene un <code>mensaje</code> esteganografiada.
     * @return            <code>String</code> con el <code>mensaje</code> que se encontraba en la <code>imagen</code>.
     */
	public static String sacarMensaje(BufferedImage imagen)
	{
		ArrayList<Integer> b_mensaje = new ArrayList<Integer>();
		char[] chars;
		for(int i = 0; i < imagen.getWidth(); i++) 
		{
			for(int j = 0; j < imagen.getHeight(); j++)
			{
				int pixel = imagen.getRGB(i, j);
				int rojo = sacarRojo(pixel);
				int verde = sacarVerde(pixel);
				int azul = sacarAzul(pixel);
				b_mensaje.add(sacarBit(rojo));
				b_mensaje.add(sacarBit(verde));
				b_mensaje.add(sacarBit(azul));
			}
		}
		chars = procesarBits(b_mensaje);
		return new String(chars);
	}

	/**
     * Devuelve un <code>int</code> que contiene el <code>bit menos significante</code> del <code>color</code> que recive.
     *
     * @param color       <code>int</code> al que se le quiere sacar el <code>bit menos significante</code>.
     * @return            <code>int</code> que contiene el <code>bit menos significante</code> del <code>color</code>.
     */
	protected static int sacarBit(int color) {
		return color & 0x00000001;
	}
	
	/**
     * Devuelve un <code>array de char</code> con el <code>mensaje</code> que se extrajo de la <code>imagen</code>.
     *
     * @param b_mensaje   <code>ArrayList</code> de Integers con todos los <code>bit menos significante</code> de cada <code>color</code> de los <code>pixeles</code> de la <code>imagen</code>.
     * @return            <code>array de char</code> que contiene todos los <code>chars</code> del <code>mensaje</code> que se extrajo.
     */
	protected static char[] procesarBits(ArrayList<Integer> b_mensaje) 
	{
		ArrayList<Integer> temp = new ArrayList<Integer>();
		
		for(int i = 0; i < b_mensaje.size(); i+=8)
		{
			int[] aux = new int[8];
			aux[0] = b_mensaje.get(i + 0);
			aux[1] = b_mensaje.get(i + 1);
			aux[2] = b_mensaje.get(i + 2);
			aux[3] = b_mensaje.get(i + 3);
			aux[4] = b_mensaje.get(i + 4);
			aux[5] = b_mensaje.get(i + 5);
			aux[6] = b_mensaje.get(i + 6);
			aux[7] = b_mensaje.get(i + 7);
					
			int letra = unirBits(aux);
						
			if((letra ^ 0xff) == 0)
				break;
			else
				temp.add(letra);
		}
		
		char[] ret = new char[temp.size()];
		for(int i = 0; i < ret.length; i++)
			ret[i] = (char) ((int) temp.get(i));
				
		return ret;
	}

	/**
     * Devuelve un <code>int</code> correspondiente a la representacion numerica de un <code>char</code>.
     *
     * @param bits        <code>array de ints</code> que contiene los <code>bits</code> que conforman un <code>char</code>.
     * @return            <code>int</code> correspondiente a la representacion numerica de un <code>char</code>.
     */
	protected static int unirBits(int[] bits) 
	{
		int ret = (bits[7] << 7) + (bits[6] << 6) + (bits[5] << 5) + (bits[4] << 4) + (bits[3] << 3) +
				(bits[2] << 2) + (bits[1] << 1) + (bits[0] << 0);
		return ret;
	}
		
}
