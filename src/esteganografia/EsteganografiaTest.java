package esteganografia;

import static org.junit.Assert.*;
import java.util.ArrayList;
import java.util.Arrays;
import org.junit.Test;

public class EsteganografiaTest 
{
	@Test
	public void testCrearBytesMensajeTrue() 
	{
		String a = "H";
		int[] b = Esteganografia.crearBytesMensaje(a);
		int[] c = {0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x01, 0x00, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01};
		assertArrayEquals(c, b);
	}
	
	@Test (expected = AssertionError.class)
	public void testCrearBytesMensajeFalseMensaje() 
	{
		String a = "M";
		int[] b = Esteganografia.crearBytesMensaje(a);
		int[] c = {0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x01, 0x00, 0x01, 0x01, 0x01, 0x01, 0x01, 0x10, 0x01, 0x01};
		assertArrayEquals(c, b);
	}

	@Test (expected = AssertionError.class)
	public void testCrearBytesMensajeFalseCola() 
	{
		String a = "H";
		int[] b = Esteganografia.crearBytesMensaje(a);
		int[] c = {0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x10, 0x00, 0x01, 0x01, 0x00, 0x01, 0x01, 0x01, 0x01, 0x01};
		assertArrayEquals(c, b);
	}
	
	@Test (expected = AssertionError.class)
	public void testCrearBytesMensajeFalseMensajeCola() 
	{
		String a = "M";
		int[] b = Esteganografia.crearBytesMensaje(a);
		int[] c = {0x00, 0x00, 0x00, 0x01, 0x01, 0x00, 0x01, 0x00, 0x01, 0x01, 0x01, 0x00, 0x01, 0x01, 0x01, 0x01};
		assertArrayEquals(c, b);
	}
	
	@Test
	public void testSacarRojoTrue() 
	{
		int pixel = 0xFF87B6A0;
		int rojo = Esteganografia.sacarRojo(pixel);
		assertTrue(rojo == 0x00000087);
	}
	
	@Test
	public void testSacarRojoFalse() 
	{
		int pixel = 0xFF87B6A0;
		int rojo = Esteganografia.sacarRojo(pixel);
		assertFalse(rojo == 0x00000088);
	}

	@Test
	public void testSacarVerdeTrue() 
	{
		int pixel = 0xFF87B6A0;
		int verde = Esteganografia.sacarVerde(pixel);
		assertTrue(verde == 0x000000B6);
	}

	@Test
	public void testSacarVerdeFalse() 
	{
		int pixel = 0xFF87B6A0;
		int verde = Esteganografia.sacarVerde(pixel);
		assertFalse(verde == 0x000000A6);
	}
	
	@Test
	public void testSacarAzulTrue() 
	{
		int pixel = 0xFF87B6A0;
		int azul = Esteganografia.sacarAzul(pixel);
		assertTrue(azul == 0x000000A0);
	}
	
	@Test
	public void testSacarAzulFalse() 
	{
		int pixel = 0xFF87B6A0;
		int azul = Esteganografia.sacarAzul(pixel);
		assertFalse(azul == 0x000000A8);
	}

	@Test
	public void testMeterBit0True() 
	{
		int color1 = 0b10001001;
		int color2 = 0b10001010;
		int res1 = Esteganografia.meterBit(color1, 0x00);
		int res2 = Esteganografia.meterBit(color2, 0x00);
		assertTrue(res1 == 0b10001000);
		assertTrue(res2 == 0b10001010);
	}
	
	@Test
	public void testMeterBit0False() 
	{
		int color1 = 0b10001001;
		int color2 = 0b10001010;
		int res1 = Esteganografia.meterBit(color1, 0x00);
		int res2 = Esteganografia.meterBit(color2, 0x00);
		assertFalse(res1 == 0b10001001);
		assertFalse(res2 == 0b10001011);
	}
	
	@Test
	public void testMeterBit1True() 
	{
		int color1 = 0b10001001;
		int color2 = 0b10001010;
		int res1 = Esteganografia.meterBit(color1, 0x01);
		int res2 = Esteganografia.meterBit(color2, 0x01);
		assertTrue(res1 == 0b10001001);
		assertTrue(res2 == 0b10001011);
	}
	
	@Test
	public void testMeterBit1False() 
	{
		int color1 = 0b10001001;
		int color2 = 0b10001010;
		int res1 = Esteganografia.meterBit(color1, 0x01);
		int res2 = Esteganografia.meterBit(color2, 0x01);
		assertFalse(res1 == 0b10001000);
		assertFalse(res2 == 0b10001010);
	}
	
	@Test
	public void testUnirColoresTrue() 
	{
		int rojo = 0x00000087;
		int verde = 0x000000B6;
		int azul = 0x000000A0;
		int pixel = Esteganografia.unirColores(rojo, verde, azul);
		assertTrue(pixel == 0xFF87B6A0);
	}
	
	@Test
	public void testUnirColoresFalse() 
	{
		int rojo = 0x00000087;
		int verde = 0x000000B6;
		int azul = 0x000000A0;
		int pixel = Esteganografia.unirColores(rojo, verde, azul);
		assertFalse(pixel == 0x0087B6A0);
	}

	@Test
	public void testSacarBitTrue() 
	{
		int color1 = 0b10001001;
		int color2 = 0b10001010;
		int res1 = Esteganografia.sacarBit(color1);
		int res2 = Esteganografia.sacarBit(color2);
		assertTrue(res1 == 0x00000001);
		assertTrue(res2 == 0x00000000);
	}

	@Test
	public void testSacarBitFalse() 
	{
		int color1 = 0b10001001;
		int color2 = 0b10001010;
		int res1 = Esteganografia.sacarBit(color1);
		int res2 = Esteganografia.sacarBit(color2);
		assertFalse(res1 == 0x00000000);
		assertFalse(res2 == 0x00000001);
	}
	
	@Test
	public void testProcesarBitsTrueLargo() 
	{
		Integer[] aux = 
				{0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x01, 0x00, 
				0x01, 0x01, 0x01, 0x01, 0x00, 0x01, 0x01, 0x00,
				0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
				0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x01, 0x00, 
				0x01, 0x01, 0x01, 0x01, 0x00, 0x01, 0x01, 0x00,
				0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01};
		ArrayList<Integer> bits = new ArrayList<Integer>(Arrays.asList(aux));
		char[] res = Esteganografia.procesarBits(bits);
		char[] valido = {'H', 'o'};
		assertArrayEquals(valido, res);
	}

	@Test
	public void testProcesarBitsTrueJusto() 
	{
		Integer[] aux = 
				{0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x01, 0x00, 
				0x01, 0x01, 0x01, 0x01, 0x00, 0x01, 0x01, 0x00,
				0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01};
		ArrayList<Integer> bits = new ArrayList<Integer>(Arrays.asList(aux));
		char[] res = Esteganografia.procesarBits(bits);
		char[] valido = {'H', 'o'};
		assertArrayEquals(valido, res);
	}
	
	@Test (expected = AssertionError.class)
	public void testProcesarBitsFalseLargo() 
	{
		Integer[] aux = 
				{0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x01, 0x00, 
				0x01, 0x01, 0x01, 0x00, 0x00, 0x01, 0x01, 0x00,
				0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
				0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x01, 0x00, 
				0x01, 0x01, 0x01, 0x01, 0x00, 0x01, 0x01, 0x00,
				0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01};
		ArrayList<Integer> bits = new ArrayList<Integer>(Arrays.asList(aux));
		char[] res = Esteganografia.procesarBits(bits);
		char[] valido = {'H', 'g'};
		char[] esperado = {'H', 'o'};
		assertArrayEquals(esperado, res);
		assertArrayEquals(valido, res);
	}

	@Test (expected = AssertionError.class)
	public void testProcesarBitsFalseJusto() 
	{
		Integer[] aux = 
				{0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x01, 0x00, 
				0x01, 0x01, 0x01, 0x00, 0x00, 0x01, 0x01, 0x00,
				0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01};
		ArrayList<Integer> bits = new ArrayList<Integer>(Arrays.asList(aux));
		char[] res = Esteganografia.procesarBits(bits);
		char[] valido = {'H', 'g'};
		char[] esperado = {'H', 'o'};
		assertArrayEquals(esperado, res);
		assertArrayEquals(valido, res);
	}
	
	@Test (expected = AssertionError.class)
	public void testProcesarBitsFalseSinCola() 
	{
		Integer[] aux = 
				{0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x01, 0x00, 
				0x01, 0x01, 0x01, 0x00, 0x00, 0x01, 0x01, 0x00,
				0x01, 0x01, 0x01, 0x00, 0x01, 0x01, 0x01, 0x00,
				0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x01, 0x00, 
				0x01, 0x01, 0x01, 0x00, 0x00, 0x01, 0x01, 0x00,
				0x01, 0x01, 0x01, 0x00, 0x01, 0x01, 0x01, 0x00};
		ArrayList<Integer> bits = new ArrayList<Integer>(Arrays.asList(aux));
		char[] res = Esteganografia.procesarBits(bits);
		char[] valido = {'H', 'g', 'w', 'H', 'g', 'w'};
		char[] esperado = {'H', 'o'};
		assertArrayEquals(esperado, res);
		assertArrayEquals(valido, res);
	}
	
	@Test
	public void testUnirBitsTrue() 
	{
		int[] a = {0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x01, 0x00};
		int res = Esteganografia.unirBits(a);
		assertTrue(res == 0x48);
	}

	@Test
	public void testUnirBitsFalse() 
	{
		int[] a = {0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x01, 0x00};
		int res = Esteganografia.unirBits(a);
		assertFalse(res == 0x22);
	}
}
