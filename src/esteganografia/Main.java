package esteganografia;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

public class Main {

	public static void main(String[] args) 
	{
		
		BufferedImage imagen = null;
		String texto = "Lorem ipsum dolor sit amet, consectetur adipiscing elit.\nCurabitur accumsan ultrices elementum.\nCras purus ex, ullamcorper vel condimentum at, interdum non nibh.\nDonec ac tortor enim.\nIn hac habitasse platea dictumst.\nNunc non quam in dolor hendrerit gravida porta ante.";
		
		try
		{
			imagen = ImageIO.read(new File("imagen/gato.png"));
			System.out.println("Carga completa.");
		}
		catch (IOException e)
		{
			System.out.println("No se encontro la imagen.");
		}		
		
		try
		{
			File f = new File("imagen/salida.png");  //output file path
			BufferedImage estego = Esteganografia.meterMensaje(imagen, texto);
			System.out.println(ImageIO.write(estego, "png", f) ? "Imagen guardada." : "Hubo un error al guardar la imagen.");
		}
		catch(IOException e)
		{
			System.out.println("Error: " + e);
		}
		
		BufferedImage prueba = null;
		
		try
		{
			prueba = ImageIO.read(new File("imagen/salida.png"));
			System.out.println("Carga completa");
		}
		catch (IOException e)
		{
			System.out.println("No se encontro la imagen.");
		}
		
		System.out.println(prueba == null ? "Hubo un error cargando la imagen." : Esteganografia.sacarMensaje(prueba));
		
	}

}
